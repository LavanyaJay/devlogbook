import {
  Command,
  FetchCommandsAction,
  AddCommandAction,
  DeleteCommandAction,
  UpdateCommandAction
} from '../actions/command';
import { ActionTypes } from '../actions/types';
export const commands = (
  state: Command[] = [],
  action:
    | FetchCommandsAction
    | AddCommandAction
    | DeleteCommandAction
    | UpdateCommandAction
) => {
  switch (action.type) {
    case ActionTypes.fetchCommands:
      return action.payload;
    case ActionTypes.addCommand:
      return [...state, action.payload];
    case ActionTypes.deleteCommand:
      return state.filter(cmd => cmd.id !== action.payload);
    case ActionTypes.updateCommand: {
      const cmdIdx = state.findIndex(cmd => cmd.id === action.payload.id);
      state[cmdIdx] = action.payload;
      return [...state];
    }
    default:
      return state;
  }
};
