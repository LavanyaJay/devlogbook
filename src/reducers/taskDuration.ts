import { fetchDurationAction } from '../actions/worklog';
import { TaskDuration } from '../models/model';
import { ActionTypes } from '../actions/types';

export const taskDuration = (state = {}, action: fetchDurationAction) => {
  switch (action.type) {
    case ActionTypes.fetchDuration: {
      console.log(action.payload);
      return action.payload;
    }
    default:
      return state;
  }
};
