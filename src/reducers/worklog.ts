import { addWorklogAction, fetchWorkLogsAction } from '../actions/worklog';
import { WorkLogsAll } from '../models/model';
import { ActionTypes } from '../actions/types';

export const worklogs = (
  state: WorkLogsAll[] = [],
  action: addWorklogAction | fetchWorkLogsAction
) => {
  switch (action.type) {
    case ActionTypes.addWorkLog: {
      return [...state, action.payload];
    }
    case ActionTypes.fetchWorkLogs: {
      return action.payload;
    }

    default:
      return state;
  }
};
