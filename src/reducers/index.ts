import { combineReducers } from 'redux';
import { commands } from './command';
import { Command } from '../actions/command';
import { tasks } from './task';
import { Task, WorkLogsAll, Notes, TaskDuration, Chart } from '../models/model';
import { worklogs } from './worklog';
import { notes } from './notes';
import { chart } from './chart';
import { taskDuration } from './taskDuration';

export interface StoreState {
  commands: Command[];
  tasks: Task[];
  worklogs: WorkLogsAll[];
  notes: Notes[];
  chart: Chart[];
  taskDuration: TaskDuration;
}

export const reducer = combineReducers({
  commands,
  tasks,
  worklogs,
  notes,
  chart,
  taskDuration
});
