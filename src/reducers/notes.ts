import { ActionTypes } from '../actions/types';
import { fetchNotesAction, updateNoteAction } from '../actions/worklog';
import { Notes } from '../models/model';

export const notes = (
  state: Notes[] = [],
  action: fetchNotesAction | updateNoteAction
) => {
  switch (action.type) {
    case ActionTypes.fetchNotes:
      return action.payload;
    case ActionTypes.updateNote: {
      const updatedState = state.map(note => {
        return note.id === action.payload.id ? action.payload : note;
      });
      return updatedState;
    }
    default:
      return state;
  }
};
