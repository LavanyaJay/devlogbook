import {
  fetchTasksAction,
  addTaskAction,
  toggleStatusAction
} from '../actions/task';
import { Task } from '../models/model';
import { ActionTypes } from '../actions/types';

export const tasks = (
  state: Task[] = [],
  action: fetchTasksAction | addTaskAction | toggleStatusAction
) => {
  switch (action.type) {
    case ActionTypes.fetchTasks:
      return action.payload;
    case ActionTypes.addTask:
      return [...state, action.payload];
    case ActionTypes.toggleStatus: {
      const updatedState = state.map(item => {
        return item.id === action.payload.id ? action.payload : item;
      });

      return updatedState;
    }
    default:
      return state;
  }
};
