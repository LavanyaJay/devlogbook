import { fetchChartAction } from '../actions/worklog';
import { Chart } from '../models/model';
import { ActionTypes } from '../actions/types';

export const chart = (state: Chart[] = [], action: fetchChartAction) => {
  switch (action.type) {
    case ActionTypes.fetchChart: {
      let effort: Chart[] = [];
      action.payload.map(data => {
        const duration = { hh: 0, mm: 0, ss: 0 };
        data.TotalDuration.hours &&
          (duration.hh = data.TotalDuration.hours * 3600);

        data.TotalDuration.minutes &&
          (duration.mm = data.TotalDuration.minutes * 60);

        data.TotalDuration.seconds &&
          (duration.ss = data.TotalDuration.seconds);
        const chart = {
          label: data.task.name,
          y: duration.hh + duration.mm + duration.ss
        };
        effort.push(chart);
      });
      return effort;
    }
    default:
      return state;
  }
};
