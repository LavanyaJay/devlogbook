import React, { Component } from 'react';
import CommandsList from './CommandsList';

import { fetchCommands, Command, deleteCommand } from '../../actions/command';
import { connect } from 'react-redux';
import { StoreState } from '../../reducers/index';
import { History } from 'history';

interface ComponentState {
  commands: Command[];
  fetchCommands: Function;
  deleteCommand: Function;
  history: History;
}

class CommandsContainer extends Component<ComponentState> {
  componentDidMount() {
    this.props.fetchCommands();
  }

  onClick = () => {
    this.props.history.push(`/addCommand`);
  };
  clickDelete = (id: Number) => {
    this.props.deleteCommand(id);
  };
  clickUpdate = (id: Number) => {
    this.props.history.push(`/addCommand/${id}`);
  };

  render() {
    return (
      <div>
        <CommandsList
          title="List of Useful Commands"
          commands={this.props.commands}
          onClick={this.onClick}
          onDelete={this.clickDelete}
          onUpdate={this.clickUpdate}
        />
      </div>
    );
  }
}

const mapStateToProps = (state: StoreState): { commands: Command[] } => {
  return { commands: state.commands };
};
export default connect(mapStateToProps, { fetchCommands, deleteCommand })(
  CommandsContainer
);
