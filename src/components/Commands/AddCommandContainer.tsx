import React, { Component } from 'react';
import AddCommand from './AddCommand';
import { Command } from '../../actions/command';
import { NewCommand } from '../../models/model';
import { addCommand, updateCommand } from '../../actions/command';
import { connect } from 'react-redux';
import { History } from 'history';
import { match } from 'react-router-dom';
import { StoreState } from '../../reducers/index';

interface Params {
  id: string;
}
interface ComponentState {
  command: NewCommand;
  history: History;
  addCommand: any;
  updateCommand: any;
  match: match<Params>;
  commands: Command[];
}

export class AddCommandContainer extends Component<ComponentState> {
  state: NewCommand = {
    subject: '',
    command: '',
    otherInfo: ''
  };
  onChangeHandler = (e: any) => {
    this.setState({ [e.target.name]: e.target.value });
  };

  onSubmitHandler = (e: React.FormEvent) => {
    e.preventDefault();
    if (this.props.match.params.id) {
      const id = this.props.match.params.id;
      this.props.updateCommand(id, this.state);
    } else {
      this.props.addCommand(this.state);
    }
    this.setState({ subject: '', command: '', otherInfo: '' });
    this.props.history.push('/commands');
  };

  componentDidMount = () => {
    const id: Number = parseInt(this.props.match.params.id);
    if (id) {
      const commandToUpdate: Command | any = this.props.commands.find(cmd => {
        return cmd.id === id;
      });
      this.setState({
        subject: commandToUpdate.subject,
        command: commandToUpdate.command,
        otherInfo: commandToUpdate.otherInfo
      });
    }
  };

  render() {
    return (
      <div>
        <AddCommand
          onChange={this.onChangeHandler}
          state={this.state}
          onSubmit={this.onSubmitHandler}
        />
      </div>
    );
  }
}
const mapStateToProps = (state: StoreState): { commands: Command[] } => {
  return {
    commands: state.commands
  };
};
export default connect(mapStateToProps, { addCommand, updateCommand })(
  AddCommandContainer
);
