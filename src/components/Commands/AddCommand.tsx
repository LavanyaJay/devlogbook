import React from 'react';
import { Form, Button } from 'react-bootstrap';
import { NewCommand } from '../../models/model';
type Props = {
  state: NewCommand;
  onChange(e: any): void;
  onSubmit(e: React.FormEvent): void;
  id?: Number;
};

export default function AddCommand(props: Props) {
  return (
    <div>
      <Form onSubmit={props.onSubmit}>
        <Form.Group>
          <Form.Label>Subject</Form.Label>
          <Form.Control
            type="text"
            placeholder="Enter subject"
            required
            name="subject"
            value={props.state.subject}
            onChange={props.onChange}
          />
        </Form.Group>

        <Form.Group>
          <Form.Label>Command</Form.Label>
          <Form.Control
            type="text"
            placeholder="Enter Code snippet"
            required
            name="command"
            value={props.state.command}
            onChange={props.onChange}
          />
        </Form.Group>
        <Form.Group>
          <Form.Label>Other Information</Form.Label>
          <Form.Control
            type="text"
            placeholder="Optional"
            name="otherInfo"
            value={props.state.otherInfo}
            onChange={props.onChange}
          />
        </Form.Group>

        <Button variant="primary" type="submit">
          Submit
        </Button>
      </Form>
    </div>
  );
}
