import React from 'react';
import { Command } from '../../actions/command';
import { Accordion, Card, Button, Table } from 'react-bootstrap';
import styled from 'styled-components';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEdit, faTrash } from '@fortawesome/free-solid-svg-icons';
import './commands.css';

const Wrapper1 = styled.div`
  background: papayawhip;
  color: palevioletred;
  margin-bottom: 1rem;
`;
const Wrapper2 = styled.div`
  color: mediumseagreen;
`;
type Props = {
  title: string;
  commands: Command[];
  onClick(): void;
  onDelete(id: Number): void;
  onUpdate(id: Number): void;
};
export default function Commands(props: Props): JSX.Element {
  return (
    <div>
      <div className="row mt-3 mb-3">
        <div className="col-xl-6">
          <h3>{props.title}</h3>
        </div>
        <div className="col-xl-6">
          <Button variant="primary" onClick={props.onClick}>
            Add New Command
          </Button>
        </div>
      </div>

      {props.commands.map(command => (
        <Accordion defaultActiveKey="0" key={command.id}>
          <Card
            style={{
              border: 'palevioletred 1px solid',
              backgroundColor: 'white'
            }}
          >
            <Card.Header
              style={{
                border: 'palevioletred 1px solid'
              }}
            >
              <Accordion.Toggle as={Button} variant="link" eventKey="1">
                <Table>
                  <tbody>
                    <tr>
                      <td
                        style={{
                          width: '55rem',
                          border: 'black 1px solid',
                          textAlign: 'left'
                        }}
                      >
                        {command.subject}
                      </td>
                      <td style={{ width: '5rem', border: 'black 1px solid' }}>
                        <FontAwesomeIcon
                          icon={faEdit}
                          onClick={() => props.onUpdate(command.id)}
                        />
                      </td>
                      <td style={{ width: '5rem', border: 'black 1px solid' }}>
                        <FontAwesomeIcon
                          icon={faTrash}
                          onClick={() => props.onDelete(command.id)}
                        />
                      </td>
                    </tr>
                  </tbody>
                </Table>
              </Accordion.Toggle>
            </Card.Header>
            <Accordion.Collapse eventKey="1">
              <Card.Body>
                <Wrapper1>{command.command}</Wrapper1>
                <Wrapper2>More Info: {command.otherInfo}</Wrapper2>
              </Card.Body>
            </Accordion.Collapse>
          </Card>
        </Accordion>
      ))}
    </div>
  );
}
