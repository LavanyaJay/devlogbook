import React, { Component } from 'react';
import { Nav, Navbar } from 'react-bootstrap';
import { Link } from 'react-router-dom';
export default class Navigation extends Component {
  render() {
    return (
      <div>
        <Navbar bg="info" expand="lg" variant="dark" sticky="top">
          <Navbar.Brand href="/">DevLogBook</Navbar.Brand>
          <Navbar.Toggle aria-controls="responsive-navbar-nav" />
          <Navbar.Collapse id="responsive-navbar-nav">
            <Link to="/">
              <Nav className="mr-3 text-dark">WorkLogs</Nav>
            </Link>
            <Link to="/commands">
              <Nav className="mr-3 text-dark"> Useful Commands </Nav>
            </Link>
            <Nav className="mr-3"> Reflections </Nav>
            <Link to="/chart">
              <Nav className="mr-3 text-dark"> Effort Chart </Nav>
            </Link>
          </Navbar.Collapse>
        </Navbar>
      </div>
    );
  }
}
