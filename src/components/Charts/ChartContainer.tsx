import React, { Component } from 'react';
import ChartDisplay from './ChartDisplay';
import { connect } from 'react-redux';
import { StoreState } from '../../reducers/index';
import { fetchChartData } from '../../actions/worklog';
import { Options, Chart } from '../../models/model';
import CanvasJSReact from '../../components/canvasjs.react';

const CanvasJSChart = CanvasJSReact.CanvasJSChart;

interface ComponentState {
  fetchChartData: Function;
  state: Options;
  options: Options | JSX.IntrinsicAttributes;
  chart: Chart[];
}
export class ChartContainer extends Component<ComponentState> {
  state: Options = {
    title: {
      text: 'Total Efforts'
    },
    axisX: {
      title: 'Tasks',
      reversed: true
    },
    axisY: {
      title: 'Duration'
    },
    data: [
      {
        type: 'bar',
        dataPoints: this.props.chart
      }
    ]
  };

  componentDidMount() {
    this.props.fetchChartData();
  }

  render() {
    return (
      <div>
        {/* <ChartDisplay options={this.state} /> */}

        <CanvasJSChart options={this.state}></CanvasJSChart>
      </div>
    );
  }
}

const mapStateToProps = (state: StoreState) => {
  console.log(state.chart);
  return {
    chart: state.chart
  };
};
export default connect(mapStateToProps, { fetchChartData })(ChartContainer);
