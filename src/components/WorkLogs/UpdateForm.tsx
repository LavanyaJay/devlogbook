import React from 'react';
import { Form, Button } from 'react-bootstrap';
type AppProps = {
  onChangeNotes(e: any): void;
  onSubmitUpdate(e: React.FormEvent): void;
  state: { id: number; notes: string };
};
export default function UpdateForm(props: AppProps) {
  return (
    <Form onSubmit={props.onSubmitUpdate}>
      <Form.Group>
        <Form.Label>Note:</Form.Label>
        <Form.Control
          name="notes"
          type="text"
          value={props.state.notes}
          onChange={props.onChangeNotes}
        ></Form.Control>
      </Form.Group>
      <Button type="submit">Update</Button>
    </Form>
  );
}
