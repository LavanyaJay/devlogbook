import * as React from 'react';
import {
  Accordion,
  Card,
  Button,
  Modal,
  Alert,
  Badge,
  Table,
  Popover,
  OverlayTrigger
} from 'react-bootstrap';
import { WorkLogsAll, Notes, TaskDuration } from '../../models/model';
import moment from 'moment';
import './worklogs.css';
//import styled from 'styled-components';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEdit, faTrash } from '@fortawesome/free-solid-svg-icons';

/* const ColourStatus = styled.h6<{ active: boolean }>`
  color: ${p => (p.active ? 'green' : 'red')};
`; */
type AppProps = {
  toggleNotes: boolean;
  workLogs: WorkLogsAll[];
  showModal(id: number): void;
  closeModal(): void;
  notes: Notes[];
  toggleStatus(id: number): void;
  updateNote(id: number, notes: string): void;
  getFormattedDuration(totalDuration: any): string;
  fetchDuration(id: number): void;
  taskDuration: TaskDuration;
  showDuration: boolean;
};

export default function WorkLogList(props: AppProps): JSX.Element {
  //let active = false;
  var popover: any;
  if (props.taskDuration.TotalDuration) {
    popover = (
      <Popover id="popover-basic">
        <Popover.Content style={{ backgroundColor: 'papayawhip' }}>
          {!props.taskDuration.TotalDuration.hours
            ? (props.taskDuration.TotalDuration.hours = 0)
            : null}
          {!props.taskDuration.TotalDuration.minutes
            ? (props.taskDuration.TotalDuration.minutes = 0)
            : null}
          {!props.taskDuration.TotalDuration.seconds
            ? (props.taskDuration.TotalDuration.seconds = 0)
            : null}
          {props.getFormattedDuration(props.taskDuration.TotalDuration)}
        </Popover.Content>
      </Popover>
    );
  } else {
    popover = (
      <Popover id="popover-basic">
        <Popover.Content>No duration</Popover.Content>
      </Popover>
    );
  }

  return (
    <div style={{ marginTop: '1rem' }}>
      {props.workLogs.length === 0 && (
        <Alert variant="danger">No worklogs added yet</Alert>
      )}
      {props.workLogs.map(log => (
        <Accordion defaultActiveKey="0" key={log.id}>
          <Card>
            <Card.Header>
              <Accordion.Toggle as={Button} variant="link" eventKey="1">
                <Table id="table1" size="md" hover responsive>
                  <tbody>
                    <tr>
                      <td>
                        <h5>{log.task.name}</h5>
                      </td>
                      <td>
                        {log.task.status === 'In Progress' ? (
                          <Badge pill variant="info">
                            <small>
                              <strong>IN PROGRESS</strong>
                            </small>
                          </Badge>
                        ) : (
                          <Badge variant="danger">
                            <small>
                              <strong>COMPLETED</strong>
                            </small>
                          </Badge>
                        )}
                      </td>
                      <td>
                        <FontAwesomeIcon icon={faTrash} />
                      </td>
                    </tr>
                  </tbody>
                </Table>

                {/* {(active = log.task.status === 'In Progress' ? true : false)}

                  <ColourStatus active={active}>{log.task.status}</ColourStatus> */}
              </Accordion.Toggle>
            </Card.Header>
            <Accordion.Collapse eventKey="1">
              <Card.Body>
                <div>
                  <h6>Complexity: {log.task.complexity}</h6>

                  <div>
                    Task started on: <strong>{log.task.startDate}</strong>
                  </div>
                  {log.task.status === 'Completed' ? (
                    <div>
                      Task completed on: <strong>{log.task.endDate}</strong>
                    </div>
                  ) : null}
                  <div>
                    <OverlayTrigger
                      trigger="click"
                      placement="right"
                      overlay={popover}
                    >
                      <Button
                        variant="warning"
                        size="sm"
                        onClick={() => props.fetchDuration(log.task.id)}
                        className="mb-1"
                      >
                        TOTAL DURATION
                      </Button>
                    </OverlayTrigger>
                  </div>

                  <Button
                    variant="info"
                    className="mr-3"
                    onClick={() => props.showModal(log.task.id)}
                  >
                    View Notes
                  </Button>
                  {props.toggleNotes && (
                    <Modal.Dialog
                      size="sm"
                      style={{
                        width: '18.5rem',
                        height: '15rem'
                      }}
                    >
                      <Modal.Body
                        style={{
                          background: 'yellow',
                          width: '18.5rem',
                          height: '11rem',
                          fontSize: '0.75rem'
                        }}
                      >
                        <Table size="sm">
                          <tbody>
                            {props.notes.map((note, index) => (
                              <tr key={index}>
                                <td>
                                  <p>
                                    <strong>
                                      {moment(note.date).format('DD-MM-YYYY')}
                                    </strong>
                                    : {note.notes}
                                  </p>
                                </td>
                                <td>
                                  <FontAwesomeIcon
                                    icon={faEdit}
                                    onClick={() =>
                                      props.updateNote(note.id, note.notes)
                                    }
                                  />
                                </td>
                              </tr>
                            ))}
                          </tbody>
                        </Table>
                      </Modal.Body>

                      <Modal.Footer
                        style={{
                          background: 'yellow'
                        }}
                      >
                        <Button
                          variant="secondary"
                          size="sm"
                          onClick={props.closeModal}
                        >
                          Close
                        </Button>
                      </Modal.Footer>
                    </Modal.Dialog>
                  )}
                  <Button
                    variant="danger"
                    as="input"
                    type="button"
                    size="sm"
                    readOnly
                    onClick={() => props.toggleStatus(log.task.id)}
                    value={
                      log.task.status === 'In Progress' ? 'COMPLETED' : 'REOPEN'
                    }
                  ></Button>
                </div>
              </Card.Body>
            </Accordion.Collapse>
          </Card>
        </Accordion>
      ))}
    </div>
  );
}
