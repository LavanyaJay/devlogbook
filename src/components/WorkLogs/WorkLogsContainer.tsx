import React, { Component } from 'react';
import WorkLogs from './WorkLogs';
import AddTask from '../Tasks/AddTask';
import DisplayTask from '../Tasks/DisplayTask';
import {
  WorkLogsAll,
  Task,
  NewTask,
  Status,
  Notes,
  TaskDuration
} from '../../models/model';
import { Button } from 'react-bootstrap';
import { StoreState } from '../../reducers/index';
import { connect } from 'react-redux';
import UpdateForm from './UpdateForm';

import { fetchTasks, addTask, toggleStatus } from '../../actions/task';
import {
  addWorkLog,
  fetchWorkLogs,
  fetchNotes,
  updateNote,
  fetchTaskDuration
} from '../../actions/worklog';
import DurationPicker from 'react-duration-picker';
import WorkLogList from './WorkLogList';
import moment from 'moment';
import { History } from 'history';

interface ComponentState {
  //state: WorkLogsAll;
  fetchTasks: Function;
  addWorkLog: Function;
  fetchWorkLogs: Function;
  tasks: Task[];
  onClickTask: Function;
  showModal: Function;
  closeModal: Function;
  task: NewTask;
  onChange: Function;
  onSubmit: Function;
  addTask: Function;
  workLogs: WorkLogsAll[];
  fetchNotes: Function;
  notes: Notes[];
  history: History;
  toggleStatus: Function;
  updateNote: Function;
  onChangeNotes: Function;
  onSubmitUpdate: Function;
  getFormattedDuration: Function;
  fetchTaskDuration: Function;
  taskDuration: TaskDuration;
}
export class WorkLogsContainer extends Component<ComponentState> {
  state = {
    taskId: 0,
    duration: {
      hours: 0,
      minutes: 0,
      seconds: 0
    },
    initialDuration: {
      hours: 0,
      minutes: 0,
      seconds: 0
    },
    date: '',
    complexity: '',
    notes: '',
    toggleModal: false,
    toggleNotes: false,
    showDuration: false,
    task: {
      name: '',
      startDate: moment(new Date()).format('YYYY-MM-DD'),
      complexity: '',
      status: Status.INPROGRESS
    },
    taskFlag: '',
    updateNoteFlag: false,
    updateNote: {
      id: 0,
      notes: ''
    }
  };

  componentDidMount() {
    this.props.fetchTasks();

    this.props.fetchWorkLogs(moment(new Date()).format('YYYY-MM-DD'));
  }

  onChangeHandler = (e: any) => {
    this.setState({ [e.target.name]: e.target.value });
  };
  onChange = (e: any): void => {
    this.setState({
      duration: { ...this.state.duration, [e.target.name]: e.target.value }
    });
  };
  onTaskChange = (e: any): void => {
    this.setState({
      task: { ...this.state.task, [e.target.name]: e.target.value }
    });
  };
  onTaskSubmit = (e: React.FormEvent): void => {
    e.preventDefault();
    this.props.addTask(this.state.task);
    this.setState({
      toggleModal: false,
      task: { ...this.state.task, name: '', complexity: '' }
    });
  };
  getFormattedDuration = (duration: {
    hours: number;
    minutes: number;
    seconds: number;
  }): string => {
    const durationStr: { hh: string; mm: string; ss: string } = {
      hh: duration.hours.toString(),
      mm: duration.minutes.toString(),
      ss: duration.seconds.toString()
    };
    console.log(durationStr);
    durationStr.hh.length === 1 &&
      (durationStr.hh === '0'
        ? (durationStr.hh = '00')
        : (durationStr.hh = '0' + durationStr.hh));
    durationStr.mm.length === 1 &&
      (durationStr.mm === '0'
        ? (durationStr.mm = '00')
        : (durationStr.mm = '0' + durationStr.mm));
    durationStr.ss.length === 1 &&
      (durationStr.ss === '0'
        ? (durationStr.ss = '00')
        : (durationStr.ss = '0' + durationStr.ss));
    const formattedDuration =
      durationStr.hh + ':' + durationStr.mm + ':' + durationStr.ss;
    return formattedDuration;
  };
  onSubmitHandler = async (e: React.FormEvent) => {
    e.preventDefault();
    const data = {
      taskId: this.state.taskId,
      duration: this.getFormattedDuration(this.state.duration),
      date: moment(new Date()).format('YYYY-MM-DD'),
      notes: this.state.notes
    };
    console.log(data);
    await this.props.addWorkLog(data);
    /*  await this.props.fetchWorkLogs(moment(new Date()).format('YYYY-MM-DD')); */

    this.setState({
      taskId: 0,
      duration: { hours: 0, minutes: 0, seconds: 0 },

      notes: ''
    });
  };
  onClickTask = () => {
    this.setState({ toggleModal: true });
  };
  showModal = (taskId: number) => {
    this.props.fetchNotes(taskId);

    this.setState({ toggleNotes: true });
  };
  closeModal = () => {
    this.setState({ toggleNotes: false });
  };
  toggleStatus = async (taskId: number) => {
    const date = moment(new Date()).format('YYYY-MM-DD');
    await this.props.toggleStatus(taskId, date);
    await this.props.fetchWorkLogs(date);
  };
  openTask = () => {
    this.setState({ taskFlag: 'open' });
  };
  closeTask = () => {
    this.setState({ taskFlag: 'closed' });
  };
  updateNote = (id: number, notes: string) => {
    this.setState({
      updateNoteFlag: true,
      updateNote: { id: id, notes: notes }
    });
  };
  onChangeNotes = (e: any) => {
    this.setState({
      updateNote: { ...this.state.updateNote, [e.target.name]: e.target.value }
    });
  };
  onSubmitUpdate = (e: React.FormEvent) => {
    e.preventDefault();

    this.props.updateNote(this.state.updateNote);
    this.setState({ updateNoteFlag: false });
  };
  fetchDuration = (id: number) => {
    this.props.fetchTaskDuration(id);
  };
  render() {
    if (this.state.updateNoteFlag) {
      return (
        <UpdateForm
          onChangeNotes={this.onChangeNotes}
          onSubmitUpdate={this.onSubmitUpdate}
          state={this.state.updateNote}
        />
      );
    }
    if (this.state.toggleModal) {
      return (
        <AddTask
          closeModal={this.closeModal}
          onChange={this.onTaskChange}
          onSubmit={this.onTaskSubmit}
          state={this.state.task}
        />
      );
    } else if (this.state.taskFlag !== '') {
      return (
        <DisplayTask
          tasks={this.props.tasks}
          toggleNotes={this.state.toggleNotes}
          showModal={this.showModal}
          closeModal={this.closeModal}
          notes={this.props.notes}
          taskFlag={this.state.taskFlag}
          toggleStatus={this.toggleStatus}
          updateNote={this.updateNote}
          fetchDuration={this.fetchDuration}
          getFormattedDuration={this.getFormattedDuration}
          taskDuration={this.props.taskDuration}
          showDuration={this.state.showDuration}
        />
      );
    } else {
      return (
        <div className="container mt-4">
          <div>
            <Button
              className=" mr-1"
              variant="warning"
              onClick={() => this.openTask()}
            >
              OpenTasks
            </Button>

            <Button variant="warning" onClick={() => this.closeTask()}>
              ClosedTasks
            </Button>
          </div>
          <div className="row mt-3">
            <div className="col-md">
              <WorkLogs
                state={this.state}
                onChangeHandler={this.onChangeHandler}
                onChange={this.onChange}
                onSubmitHandler={this.onSubmitHandler}
                tasks={this.props.tasks}
                workLogs={this.props.workLogs}
                onClickTask={this.onClickTask}
              />
            </div>
            <div className="col-md ml-4">
              <h3 style={{ textAlign: 'center' }}>WorkLog for the Day</h3>
              <div style={{ border: '1px solid black' }} className="mt-4">
                <WorkLogList
                  workLogs={this.props.workLogs}
                  toggleNotes={this.state.toggleNotes}
                  showModal={this.showModal}
                  closeModal={this.closeModal}
                  notes={this.props.notes}
                  toggleStatus={this.toggleStatus}
                  updateNote={this.updateNote}
                  fetchDuration={this.fetchDuration}
                  getFormattedDuration={this.getFormattedDuration}
                  taskDuration={this.props.taskDuration}
                  showDuration={this.state.showDuration}
                />
              </div>
            </div>
          </div>
        </div>
      );
    }
  }
}
const mapStateToProps = (state: StoreState) => {
  console.log(state.worklogs);
  return {
    tasks: state.tasks,
    workLogs: state.worklogs,
    notes: state.notes,
    taskDuration: state.taskDuration
  };
};
export default connect(mapStateToProps, {
  fetchTasks,
  addWorkLog,
  addTask,
  fetchWorkLogs,
  fetchNotes,
  toggleStatus,
  updateNote,
  fetchTaskDuration
})(WorkLogsContainer);
