import React from 'react';
import DurationPicker from 'react-duration-picker';
import { Form, Button, Col } from 'react-bootstrap';
import { Worklog, Task, WorkLogsAll, Duration } from '../../models/model';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPlusCircle } from '@fortawesome/free-solid-svg-icons';

interface Props {
  state: Worklog;
  workLogs: WorkLogsAll[];
  tasks: Task[];
  onChangeHandler(e: any): void;
  onChange(e: any): void;
  onSubmitHandler(e: React.FormEvent): void;
  onClickTask(): void;
}
export default function WorkLogs(props: Props): JSX.Element {
  const buildHours = () => {
    var hhArray = [];
    for (let i = 0; i < 9; i++) {
      hhArray.push(<option key={i}>{i}</option>);
    }
    return hhArray;
  };

  const buildMinutes = () => {
    var mmArray = [];
    for (let i = 0; i < 60; i += 5) {
      mmArray.push(<option key={i}>{i}</option>);
    }
    return mmArray;
  };
  const buildSeconds = () => {
    var ssArray = [];
    for (let i = 0; i < 60; i += 5) {
      ssArray.push(
        <option key={i} value={props.state.duration.seconds}>
          {i}
        </option>
      );
    }
    return ssArray;
  };
  const todayTaskIdList = props.workLogs.map(logs => {
    return logs.taskId;
  });

  const displayTasks = props.tasks.filter(
    task =>
      task.status !== 'Completed' && todayTaskIdList.indexOf(task.id) === -1
  );

  return (
    <div>
      <h3 style={{ textAlign: 'center' }}>Add WorkLog</h3>
      <Form onSubmit={props.onSubmitHandler}>
        <Form.Group>
          <Form.Label>
            Task
            <sup>
              <FontAwesomeIcon
                icon={faPlusCircle}
                onClick={props.onClickTask}
              />
            </sup>
          </Form.Label>
          <Form.Control
            as="select"
            placeholder="Select Task"
            required
            name="taskId"
            value={props.state.taskId}
            onChange={props.onChangeHandler}
          >
            <option></option>
            {displayTasks.map((task, index) => {
              return (
                <option key={task.id} value={task.id}>
                  {task.name}
                </option>
              );
            })}
          </Form.Control>
        </Form.Group>

        <Form.Group>
          <Form.Label>Duration</Form.Label>
          {/* <DurationPicker
            onChange={props.onChange}
            initialDuration={{
              hours: props.state.duration.hours,
              minutes: props.state.duration.minutes,
              seconds: props.state.duration.seconds
            }}
          /> */}
          <Form.Row>
            <Col>
              <Form.Control
                as="select"
                type="number"
                required
                name="hours"
                value={props.state.duration.hours}
                onChange={props.onChange}
                size="sm"
              >
                <option></option>
                {buildHours()}
              </Form.Control>
            </Col>
            :
            <Col>
              <Form.Control
                as="select"
                type="number"
                required
                name="minutes"
                value={props.state.duration.minutes}
                onChange={props.onChange}
                size="sm"
              >
                <option></option>
                {buildMinutes()}
              </Form.Control>
            </Col>
            :
            <Col>
              <Form.Control
                as="select"
                type="number"
                required
                name=""
                value={props.state.duration.seconds}
                onChange={props.onChange}
                size="sm"
              >
                <option></option>
                {buildSeconds()}
              </Form.Control>
            </Col>
          </Form.Row>
        </Form.Group>

        <Form.Group>
          <Form.Label>Notes</Form.Label>
          <Form.Control
            type="text"
            as="textarea"
            placeholder="Enter Notes"
            name="notes"
            value={props.state.notes}
            onChange={props.onChangeHandler}
          />
        </Form.Group>

        <Button variant="primary" type="submit">
          Submit
        </Button>
      </Form>
    </div>
  );
}
