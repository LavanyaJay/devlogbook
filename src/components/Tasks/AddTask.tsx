import { Modal, Form, Button } from 'react-bootstrap';
import React from 'react';
import { NewTask } from '../../models/model';
interface Props {
  closeModal(): void;
  onChange(e: any): void;
  onSubmit(e: any): void;
  state: NewTask;
}
export default function AddTask(props: Props) {
  return (
    <div>
      <Modal.Dialog>
        <Modal.Header closeButton>
          <Modal.Title>Add New task</Modal.Title>
        </Modal.Header>

        <Modal.Body>
          <Form onSubmit={props.onSubmit}>
            <Form.Group>
              <Form.Label>Name</Form.Label>
              <Form.Control
                type="text"
                placeholder="Enter Task Name"
                name="name"
                value={props.state.name}
                required
                onChange={props.onChange}
              ></Form.Control>
            </Form.Group>
            <Form.Group>
              <Form.Label>Complexity</Form.Label>
              <Form.Control
                as="select"
                placeholder="Enter Complexity"
                name="complexity"
                value={props.state.complexity}
                onChange={props.onChange}
              >
                <option></option>
                <option>High</option>
                <option>Medium</option>
                <option>Low</option>
              </Form.Control>
            </Form.Group>
            <Button type="submit">Add Task</Button>
          </Form>
        </Modal.Body>

        <Modal.Footer>
          <Button variant="secondary" onClick={props.closeModal}>
            Close
          </Button>
        </Modal.Footer>
      </Modal.Dialog>
    </div>
  );
}
