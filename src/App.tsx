import React from 'react';
import { Route } from 'react-router-dom';
import CommandsContainer from './components/Commands/CommandsContainer';
import AddCommandContainer from './components/Commands/AddCommandContainer';
import WorkLogsContainer from './components/WorkLogs/WorkLogsContainer';
import ChartContainer from './components/Charts/ChartContainer';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import Navigation from './components/Navigation';
import { Container } from 'react-bootstrap';
import { Provider } from 'react-redux';
import store from './store';
class App extends React.Component {
  render() {
    return (
      <Provider store={store}>
        <Container>
          <Navigation />
          <Route exact path="/" component={WorkLogsContainer}></Route>
          <Route exact path="/commands" component={CommandsContainer}></Route>
          <Route
            exact
            path="/addCommand"
            component={AddCommandContainer}
          ></Route>
          <Route
            exact
            path="/addCommand/:id"
            component={AddCommandContainer}
          ></Route>
          <Route exact path="/chart" component={ChartContainer}></Route>
        </Container>
      </Provider>
    );
  }
}

export default App;
