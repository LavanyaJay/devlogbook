export interface NewCommand {
  subject: string;
  command: string;
  otherInfo: string;
}

export interface Duration {
  hours: number;
  minutes: number;
  seconds: number;
}

export interface Worklog {
  taskId: number;
  duration: Duration;
  initialDuration: Duration;
  notes: string;
  date: string;
}
export interface WorkLog {
  taskId: number;
  duration: String;
  date: string;
  notes: string;
}
export interface WorkLogsAll {
  id: number;
  task: Task;
  duration: String;
  date: string;
  notes: string;
  taskId: number;
}
export enum Status {
  INPROGRESS = 'In Progress',
  COMPLETED = 'Completed'
}
export interface Task {
  id: number;
  name: string;
  startDate: string;
  endDate: string;
  complexity: string;
  status: Status;
}
export interface NewTask {
  name: string;
  startDate: string;
  status: Status;
  complexity: string;
}

export interface Notes {
  id: number;
  notes: string;
  date: Date;
}

export interface ChartData {
  TotalDuration: {
    hours?: number;
    minutes?: number;
    seconds?: number;
  };
  task: {
    name: string;
  };
  taskId: number;
}
export interface Chart {
  label: string;
  y: number;
}

export interface Options {
  title: {
    text: string;
  };
  axisX: {
    title: 'Tasks';
    reversed: true;
  };
  axisY: {
    title: 'Duration';
  };
  data: {
    type: string;

    dataPoints: Chart[];
  }[];
}

export interface TaskDuration {
  TotalDuration: {
    hours?: number;
    minutes?: number;
    seconds?: number;
  };
  task: {
    name: string;
  };
  taskId: number;
}
