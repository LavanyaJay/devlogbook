import { ActionTypes } from './types';
import { Dispatch } from 'redux';
import axios from 'axios';
import {
  WorkLog,
  WorkLogsAll,
  Notes,
  ChartData,
  TaskDuration
} from '../models/model';
import { url } from '../constants';

export interface addWorklogAction {
  type: ActionTypes.addWorkLog;
  payload: WorkLog;
}
export interface fetchWorkLogsAction {
  type: ActionTypes.fetchWorkLogs;
  payload: WorkLogsAll[];
}

export interface fetchNotesAction {
  type: ActionTypes.fetchNotes;
  payload: Notes[];
}

export interface updateNoteAction {
  type: ActionTypes.updateNote;
  payload: Notes;
}

export interface fetchChartAction {
  type: ActionTypes.fetchChart;
  payload: ChartData[];
}

export interface fetchDurationAction {
  type: ActionTypes.fetchDuration;
  payload: TaskDuration;
}

export const addWorkLog = (data: WorkLog) => {
  return async (dispatch: Dispatch) => {
    const response = await axios.post(`${url}/worklog`, data);
    dispatch<addWorklogAction>({
      type: ActionTypes.addWorkLog,
      payload: response.data
    });
  };
};

export const fetchWorkLogs = (date: string) => {
  console.log(date);
  return async (dispatch: Dispatch) => {
    const response = await axios.get(`${url}/worklog/${date}`);
    console.log(response.data);
    dispatch<fetchWorkLogsAction>({
      type: ActionTypes.fetchWorkLogs,
      payload: response.data
    });
  };
};

export const fetchNotes = (taskId: Number) => {
  return async (dispatch: Dispatch) => {
    const response = await axios.get(`${url}/worklog/${taskId}/notes`);
    dispatch<fetchNotesAction>({
      type: ActionTypes.fetchNotes,
      payload: response.data
    });
  };
};

export const updateNote = (updatedNote: { id: number; notes: string }) => {
  return async (dispatch: Dispatch) => {
    const data = {
      notes: updatedNote.notes
    };
    const response = await axios.patch(
      `${url}/worklog/${updatedNote.id}`,
      data
    );
    dispatch<updateNoteAction>({
      type: ActionTypes.updateNote,
      payload: response.data
    });
  };
};

export const fetchChartData = () => {
  return async (dispatch: Dispatch) => {
    const response = await axios.get(`${url}/getChart`);
    dispatch<fetchChartAction>({
      type: ActionTypes.fetchChart,
      payload: response.data
    });
  };
};

export const fetchTaskDuration = (id: number) => {
  return async (dispatch: Dispatch) => {
    const response = await axios.get(`${url}/totalDuration/${id}`);
    dispatch<fetchDurationAction>({
      type: ActionTypes.fetchDuration,
      payload: response.data
    });
  };
};
