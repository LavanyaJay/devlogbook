import { ActionTypes } from './types';
import { Task } from '../models/model';
import { Dispatch } from 'redux';
import axios from 'axios';
import { url } from '../constants';

export interface fetchTasksAction {
  type: ActionTypes.fetchTasks;
  payload: Task[];
}
export interface addTaskAction {
  type: ActionTypes.addTask;
  payload: Task;
}

export interface toggleStatusAction {
  type: ActionTypes.toggleStatus;
  payload: Task;
}

export const fetchTasks = () => {
  return async (dispatch: Dispatch) => {
    const response = await axios.get(`${url}/task`);
    dispatch<fetchTasksAction>({
      type: ActionTypes.fetchTasks,
      payload: response.data
    });
  };
};

export const addTask = (data: Task) => {
  return async (dispatch: Dispatch) => {
    const response = await axios.post(`${url}/task`, data);
    console.log(response.data);
    dispatch<addTaskAction>({
      type: ActionTypes.addTask,
      payload: response.data
    });
  };
};

export const toggleStatus = (taskId: Number, date: string) => {
  return async (dispatch: Dispatch) => {
    const data = { date: date };
    const response = await axios.patch(
      `${url}/task/${taskId}/toggleStatus`,
      data
    );
    dispatch<toggleStatusAction>({
      type: ActionTypes.toggleStatus,
      payload: response.data
    });
  };
};
