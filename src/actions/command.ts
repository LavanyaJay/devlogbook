import axios from 'axios';
import { Dispatch } from 'redux';
import { url } from '../constants';
import { ActionTypes } from './types';
import { NewCommand } from '../models/model';
export interface Command {
  id: number;
  subject: string;
  command: string;
  otherInfo: string;
}
export interface FetchCommandsAction {
  type: ActionTypes.fetchCommands;
  payload: Command[];
}
export interface AddCommandAction {
  type: ActionTypes.addCommand;
  payload: NewCommand;
}
export interface DeleteCommandAction {
  type: ActionTypes.deleteCommand;
  payload: Number;
}
export interface UpdateCommandAction {
  type: ActionTypes.updateCommand;
  payload: Command;
}
export const fetchCommands = () => {
  return async (dispatch: Dispatch) => {
    const response = await axios.get<Command[]>(`${url}/commands`);
    dispatch<FetchCommandsAction>({
      type: ActionTypes.fetchCommands,
      payload: response.data
    });
  };
};

export const addCommand = (data: NewCommand) => {
  return async (dispatch: Dispatch) => {
    const response = await axios.post<Command>(`${url}/command`, data);

    dispatch<AddCommandAction>({
      type: ActionTypes.addCommand,
      payload: response.data
    });
  };
};

export const deleteCommand = (id: Number) => {
  return async (dispatch: Dispatch) => {
    const response = await axios.delete<Number>(`${url}/command/${id}`);
    if (response.data) {
      dispatch<DeleteCommandAction>({
        type: ActionTypes.deleteCommand,
        payload: id
      });
    }
  };
};

export const updateCommand = (id: Number, data: NewCommand) => {
  return async (dispatch: Dispatch) => {
    console.log(data);
    const response = await axios.patch(`${url}/command/${id}`, data);

    dispatch<UpdateCommandAction>({
      type: ActionTypes.updateCommand,
      payload: response.data
    });
  };
};
