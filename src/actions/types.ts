export enum ActionTypes {
  fetchCommands = 'FETCH_COMMANDS',
  addCommand = 'ADD_COMMAND',
  deleteCommand = 'DELETE_COMMAND',
  updateCommand = 'UPDATE_COMMAND',
  fetchTasks = 'FETCH_TASKS',
  addTask = 'ADD_TASK',
  addWorkLog = 'ADD_WORKLOG',
  fetchWorkLogs = 'FETCH_WORKLOGS',
  fetchNotes = 'FETCH_NOTES',
  toggleStatus = 'TOGGLE_STATUS',
  fetchChart = 'FETCH_CHART',
  updateNote = 'UPDATE_NOTE',
  fetchDuration = 'FETCH_DURATION'
}
