## LogBook - Task and Effort tracker

Check out the remotely-hosted [website](https://devlogbook.netlify.app/)!

## Table of contents

- [Preface](#Preface)
- [How to start](#How-to-start)
- [Technologies used for this project](#Technologies-used-for-this-project)
- [Contributors](#Contributors)

## Preface

As I commence my journey as a Front end developer, this application is to help me to record

- Tasks undertaken for the day
- Efforts spent on the tasks
- Code snippets
- My reflections as a software developer

## How to start

1. Clone the git repository into a new directory on your computer: `git clone https://LavanyaJay@bitbucket.org/LavanyaJay/devlogbook.git`
2. Run `npm install` on your terminal to install all the dependendencies
3. Run `npm start` to get a preview of the front-end

## Still to do

- Delete functionality

- **_HAVE FUN!!!_**

## Technologies used for this project

1. Typescript with React with `npm create-react-app my-app --template typescript`
2. `redux` and `react-redux` to set up a redux store and dispatch actions
3. `react-router` and `react-router-dom` to use routes in react and have dom elements that work with them
4. `axios` to fetch data from the database
5. `redux-thunk` to dispatch actions for the redux store

## Contributors

- Lavanya Jayapalan | [Bit Bucket](https://bitbucket.org/LavanyaJay)
